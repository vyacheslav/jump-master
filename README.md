# Jump Master
### Reach for the stars !

Jump Master is a simple single-player 2D platforming game developed with Unity.
The main goal of the game is to get the highest score possible by continually
jumping on platforms to reach the highest possible height. There are several
different types of platforms and enemies.

## Credits
All assets including: Sprites, sounds and images were taken from free sources.

Platform Sprites : https://foxlybr.itch.io/23421124244
Character Sprites: https://lionheart963.itch.io/free-platformer-assets
Menu Music: www.freemusicarchive.org
Jumping Sounds: https://freesound.org/

## Mockups
![Player Screen](https://framagit.org/vyacheslav/jump-master/raw/6ffe9150edfe0e7317cc478e4bf96887a8f63a27/Proposal/Mockups/JumpMaster_PlayScreen1.PNG)
![Menu Screen](https://framagit.org/vyacheslav/jump-master/raw/6ffe9150edfe0e7317cc478e4bf96887a8f63a27/Proposal/Mockups/JumpMaster_MenuScreen.PNG)
![Death Screen](https://framagit.org/vyacheslav/jump-master/raw/6ffe9150edfe0e7317cc478e4bf96887a8f63a27/Proposal/Mockups/JumpMaster_DeathScreen.PNG)

## Target users
The game we are creating is intended for all ages and hopes to reach as large of an audience as possible. The game will be accesible to anyone
and will be playable at any time.

## Alternatives
Since our game is based on Doodle Jump, there are multiple platforming games that have the same core mechanics. This is our own take on this
type of game, and we intend to make it as fun as possible. Alternatives include:

1. Doodle Jump
2. Reach the Sky
3. Ball Bouncer
4. Poodle Jump
5. Mega Jump

## Usage 
The game's basic layout is as follows:

1. The player is greeted with the Main Menu screen where they can either start the game, enter the options menu or review their highscores.
2. When the game begins, the player automatically begins jumping on the platforms and does not stop until the player character dies by falling.
3. When the player dies he is faced with the Game Over screen and can either choose to replay the game or go back to the Main menu.
4. In the options menu, the player can change their player sprite, turn the sound on or off and toggle enemies on or off.
5. In the high score screen, the player will be able to review their 5 highest scores throughout their play sessions.

## Roadmap

### Milestone 1
In this milestone, we would like to have all the prerequisites for the project setup, all of the assets for the game to be present in our repository.
The basic game mechanics should all be working as intended. The core game play needs to be functional. E.g: jumping on platforms, player death, player movement, physics for all objects, score.

#1 **Add Objects/Physics**

#2 **Jumping Mechanics**

#3 **Player Movement**

#4 **Death and Respawn**

#5 **Scoreboard**

### Milestone 2
In this milestone, we would like to have all of the menus like the start screen, the game over screen and the settings screen. Additionally, we would like to implement a pause feature.

#6 **Start Menu**

#7 **Settings Screen**

#8 **Pause Feature**

#9 **Game Over Screen**

#10 **High Score Screen**

### Milestone 3
In this milestone, we would like to polish the game up as much as possible. This means, making the game be devoid of any crashes or bugs, adding more types of enemies and platforms. Adding items, adding obstacles/boosts(i.e trampolines). Basically, cleaning up the game/making it more feature-packed.

#11 **Squashing Bugs**

#12 **Add More Enemies**

#13 **Adding Items**

#14 **Add New Platforms**

#15 **Sound**

## Installation

### For running Unity games
Generally content developed with Unity can run pretty much everywhere. How well it runs is dependent on the complexity of your project. More detailed requirements:

- iOS player requires iOS 9.0 or higher.
- Android: OS 4.1 or later; ARMv7 CPU with NEON support or Atom CPU; OpenGL ES 2.0 or later.

To test the app:

- Download the APK file "Jump-Master" from this repository
- Once the download is done, Open a generic MyApplication project in Android Studio
- Click the Run button to open an emulator. We use the Nexus 5X API 28 x86 for our tests
- Drag and drop the APK file into the emulator to install the game 
- Open the app and enjoy 
 

## Authors

- Vyacheslav Gorbov
- Earl Jaedrick Palisoc
- Julien Hashim

## License

[MIT License](LICENSE)
