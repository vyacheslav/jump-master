﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsSelector : MonoBehaviour
{
    public GameObject soundOptions;
    public GameObject enemiesOptions;
    public GameObject gravityOptions;

    // Start is called before the first frame update
    void Start()
    {
        Toggle soundToggle = soundOptions.GetComponent<Toggle>();
        if (PlayerPrefs.GetString("SoundToggle") == "False")
            soundToggle.isOn = false;

        Toggle enemiesToggle = enemiesOptions.GetComponent<Toggle>();
        if (PlayerPrefs.GetString("EnemiesToggle") == "False")
            enemiesToggle.isOn = false;

        Slider gravitySlider = gravityOptions.GetComponent<Slider>();
        if (PlayerPrefs.GetInt("GravitySlider") == 1)
            gravitySlider.value = 1;
        else if (PlayerPrefs.GetInt("GravitySlider") == 3)
            gravitySlider.value = 3;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        if (PlayerPrefs.GetString("SoundToggle") == "False")
            AudioListener.volume = 0f;
        else
            AudioListener.volume = 1f;
    }
}
