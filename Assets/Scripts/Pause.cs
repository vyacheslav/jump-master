﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause : MonoBehaviour
{
    public static bool IsPaused = false;
    public Button myButton;
    public Sprite PlayImage;
    public Sprite PauseImage;

    public void PauseGame()
    {
        IsPaused = !IsPaused;

        if (!IsPaused)
        {
            Time.timeScale = 1;
            myButton.image.sprite = PauseImage;
        } 
        else if (IsPaused)
        {
            Time.timeScale = 0;
            myButton.image.sprite = PlayImage;
        }
    }

     
}
