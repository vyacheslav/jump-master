﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyPlatform : MonoBehaviour
{
    public GameObject levelGenerator;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "RegularPlatform")
        {
            Destroy(collision.gameObject);
            levelGenerator.GetComponent<LevelGenerator>().SpawnPlatform();
        }

        if (collision.gameObject.tag == "TrampolinePlatform")
        {
            Destroy(collision.gameObject);
            levelGenerator.GetComponent<LevelGenerator>().SpawnPlatform();
        }

        if (collision.gameObject.tag == "BrokenPlatform")
        {
            Destroy(collision.gameObject);
            levelGenerator.GetComponent<LevelGenerator>().SpawnPlatform();
        }

        if (collision.gameObject.tag == "FlyingBat")
        {
            Destroy(collision.gameObject);
        }
    }
}
