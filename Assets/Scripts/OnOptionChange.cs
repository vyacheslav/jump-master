﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnOptionChange : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnValueChanged()
    {
        try
        {
            string newPref = this.GetComponent<Toggle>().isOn.ToString();
            PlayerPrefs.SetString(this.name, newPref);
            PlayerPrefs.Save();
            if (newPref == "False")
                this.GetComponentInChildren<Text>().text = "OFF";
            else
                this.GetComponentInChildren<Text>().text = "ON";
        } catch (Exception)
        {
            int newPref = (int)this.GetComponent<Slider>().value;
            PlayerPrefs.SetInt(this.name, newPref);
            PlayerPrefs.Save();
        }
    }
}
