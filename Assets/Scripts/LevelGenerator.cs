﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public GameObject platformPrefab;
    public GameObject trampolinePrefab;
    public GameObject brokenPlatformPrefab;
    public GameObject leftWall;
    public GameObject rightWall;
    public int numberOfPlatforms = 50;
    private float levelWidth;
    private float minY = 1f;
    private float maxY = 4f;
    private Vector3 spawnPosition;
    private Vector3 leftWallVector;
    private Vector3 rightWallVector;

    private const int PLATFORM_PIXEL_WIDTH = 240;
    private const float PLATFORM_X_SCALE = 0.8f;
    private const int PLATFORM_RPPU = 100;

    // Start is called before the first frame update
    void Start()
    {
        // adjust min and max x coordinates of platforms according to screen size
        levelWidth = Camera.main.ViewportToWorldPoint(Camera.main.transform.position).x
            - (float)((PLATFORM_PIXEL_WIDTH / 2) * PLATFORM_X_SCALE / PLATFORM_RPPU);

        // adjust x coordinates of left and right walls according to screen size
        leftWallVector = new Vector3(-Camera.main.ViewportToWorldPoint(Camera.main.transform.position).x, 0, 0);
        leftWall.transform.position += leftWallVector;
        rightWallVector = new Vector3(Camera.main.ViewportToWorldPoint(Camera.main.transform.position).x, 0, 0);
        rightWall.transform.position += rightWallVector;

        // spawn platforms
        spawnPosition = new Vector3();
        for (int i = 0; i < numberOfPlatforms; i++)
        {
            SpawnPlatform();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnPlatform()
    {
        int platformRandomizer = Random.Range(1, 10); 
        if (platformRandomizer > 2)
        {
            spawnPosition.y += Random.Range(minY, maxY);
            spawnPosition.x = Random.Range(-levelWidth, levelWidth);
            Instantiate(platformPrefab, spawnPosition, Quaternion.identity);
            platformPrefab.tag = "RegularPlatform";
        }
        else if (platformRandomizer == 2)
        {
            spawnPosition.y += Random.Range(minY, maxY);
            spawnPosition.x = Random.Range(-levelWidth, levelWidth);
            Instantiate(brokenPlatformPrefab, spawnPosition, Quaternion.identity);
            brokenPlatformPrefab.tag = "BrokenPlatform";
        }
        else
        {
            spawnPosition.y += Random.Range(minY, maxY);
            spawnPosition.x = Random.Range(-levelWidth, levelWidth);
            Instantiate(trampolinePrefab, spawnPosition, Quaternion.identity);
            trampolinePrefab.tag = "TrampolinePlatform";
        }
    }
}
