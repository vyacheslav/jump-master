﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class HighscoreTable : MonoBehaviour
{
    private Transform entryContainer;
    private Transform entryTemplate;
    private List<Transform> highscoreEntryTransformList;

    private void Awake()
    {
        // Get the container and template made
        entryContainer = transform.Find("highscoreEntryContainer");
        entryTemplate = entryContainer.Find("highscoreEntryTemplate");

        entryTemplate.gameObject.SetActive(false);

        if (!PlayerPrefs.HasKey("highscoreTable"))
        {
            GenerateRandomEntries();           
        }

        // Call add method, inserts the most recent game session score
        AddHighscoreEntry(PlayerPrefs.GetFloat("CurrentScore"));

        //Load highscoretable
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        //Sort entry list by score
        for (int i = 0; i < highscores.highscoreEntryList.Count; i++)
        {
            for (int j = i + 1; j < highscores.highscoreEntryList.Count; j++)
            {
                // Swap
                if (highscores.highscoreEntryList[j].score > highscores.highscoreEntryList[i].score)
                {
                    float temp = highscores.highscoreEntryList[i].score;
                    highscores.highscoreEntryList[i].score = highscores.highscoreEntryList[j].score;
                    highscores.highscoreEntryList[j].score = temp; 
                }
            }
        }

        // Removes the lowest score 
        while (highscores.highscoreEntryList.Count > 5)
        {
            highscores.highscoreEntryList.RemoveAt(5);
        }

        // Creates the highscore board with entries
        highscoreEntryTransformList = new List<Transform>();
        foreach (HighscoreEntry highscoreEntry in highscores.highscoreEntryList)
        {
            CreateHighscoreEntryTransform(highscoreEntry, entryContainer, highscoreEntryTransformList);
        } 
    }

    private void GenerateRandomEntries()
    {
        //Load highscoretable
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        // Highscores setHighscores = new Highscores { highscoreEntryList = new List<HighscoreEntry>()};
        Highscores setHighscores = new Highscores();

        List<HighscoreEntry> highscoreEntryList = new List<HighscoreEntry>()
        {
            new HighscoreEntry{ score = 0 },
            new HighscoreEntry{ score = 0 },
            new HighscoreEntry{ score = 0 },
            new HighscoreEntry{ score = 0 },
            new HighscoreEntry{ score = 0 },
        };

        setHighscores.highscoreEntryList = highscoreEntryList;

        highscores = setHighscores;

        // Save updated Highscores
        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("highscoreTable", json);
        PlayerPrefs.Save();
    }

    /**
     * Method creating the high score entry. Will take the entry, the container, and a list that will hold
     * the instantiated transform entries
     * */
    private void CreateHighscoreEntryTransform(HighscoreEntry highscoreEntry, Transform container, List<Transform> transformList)
    {
        float templateHeight = 20f;
        // Must instantiate so that entryTransform can get the the components/place the entries correctly 
        Transform entryTransform = Instantiate(entryTemplate, container);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, -templateHeight * transformList.Count);
        entryTransform.gameObject.SetActive(true);

        // Code below + switch case is to generate the Rank text
        int rank = transformList.Count + 1;
        string rankString;

        switch (rank)
        {
            default:
                rankString = rank + "TH";
                break;
            case 1:
                rankString = "1ST";
                break;
            case 2:
                rankString = "2ND";
                break;
            case 3:
                rankString = "3RD";
                break;
        }

        // Generates the rank
        entryTransform.Find("rank").GetComponent<Text>().text = rankString;

        // Generates the score 
        float score = highscoreEntry.score;
        entryTransform.Find("score").GetComponent<Text>().text = score.ToString();

        // Forms the entry and adds it to the list
        transformList.Add(entryTransform); 
    }

    private void AddHighscoreEntry (float score)
    {
        // Create HighscoreEntry
        HighscoreEntry highscoreEntry = new HighscoreEntry { score = (float) Math.Round(score)};

        // Load saved Highscores
        string jsonString = PlayerPrefs.GetString("highscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        // Add new entry to Highscores
        highscores.highscoreEntryList.Add(highscoreEntry);

        // Save updated Highscores
        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("highscoreTable", json);
        PlayerPrefs.Save();
    }

    // Class with a variable that makes up the list of highscores
    private class Highscores
    {
        public List<HighscoreEntry> highscoreEntryList;
    }

    // Entry class that has the score variable
    [System.Serializable]
    private class HighscoreEntry
    {
        public float score; 
    }
}
