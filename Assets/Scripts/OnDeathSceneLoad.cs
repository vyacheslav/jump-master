﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OnDeathSceneLoad : MonoBehaviour
{
    public GameObject youDiedText;
    public GameObject player;
    public Scene deathScene;
    public Canvas canvas;

    public float playerX;
    public float currentScore;
    public Button playButton;
    public Button menuButton;

    // Start is called before the first frame update
    void Start()
    {
        youDiedText.gameObject.SetActive(false);
        playButton.gameObject.SetActive(false);
        menuButton.gameObject.SetActive(false);
        playerX = PlayerPrefs.GetFloat("CurrentX");
        currentScore = PlayerPrefs.GetFloat("CurrentScore");
        player.transform.position = new Vector3(playerX, 5.3f);
    }
}
