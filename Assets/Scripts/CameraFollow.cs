﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraFollow : MonoBehaviour
{

    public Transform target;
    public Player player;

    public Transform rightCollider;
    public Transform leftCollider;
    public Vector2 screenBounds; 

    public float levelWidth = (float)Screen.width;
    // Use this for initialization

    // Update is called once per frame
    void Update()
    {
        //Make them the child of whatever object this script is on, preferably on the Camera so the objects move with the camera without extra scripting
        rightCollider.parent = transform;
        leftCollider.parent = transform;

        if (target.position.y > transform.position.y)
        {
            transform.position = new Vector3(transform.position.x, target.position.y, transform.position.z);
        }
        
        if (target.position.y < transform.position.y && player.isDead)
        {
            transform.position = new Vector3(transform.position.x, target.position.y, transform.position.z);
            player.isDead = false;
        }
    }
}
