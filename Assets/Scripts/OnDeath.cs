﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnDeath : MonoBehaviour
{
    public GameObject youDiedText;
    public Text scoreText;
    public Button playButton;
    public Button menuButton;

    void OnCollisionEnter2D(Collision2D collision)
    {
        youDiedText.gameObject.SetActive(true);
        playButton.gameObject.SetActive(true);
        menuButton.gameObject.SetActive(true);
        scoreText.text = "Score: " + Mathf.Round(PlayerPrefs.GetFloat("CurrentScore")).ToString();
    }
}
