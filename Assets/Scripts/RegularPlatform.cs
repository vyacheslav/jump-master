﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegularPlatform : MonoBehaviour
{

    private float jumpForce;

    private void Start()
    {
        if (!PlayerPrefs.HasKey("GravitySlider"))
            jumpForce = 18f;
        else
        {
            switch (PlayerPrefs.GetInt("GravitySlider"))
            {
                case 1: jumpForce = 16f;
                    break;
                case 2: jumpForce = 18f;
                    break;
                case 3: jumpForce = 20f;
                    break;
                default:
                    jumpForce = 18f;
                    break;
            }
        }

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.relativeVelocity.y <= 0f)
        {
            Rigidbody2D rb = collision.collider.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                Vector2 velocity = rb.velocity;
                velocity.y = jumpForce;
                rb.velocity = velocity;
                GetComponent<AudioSource>().Play();
            }
        }
    }
}
