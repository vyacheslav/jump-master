﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokenPlatform : MonoBehaviour
{

    private float jumpForce = 18f;
    public GameObject brokenPlatform;
    public AudioSource jumpSound;
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.relativeVelocity.y <= 0f)
        {
            Rigidbody2D rb = collision.collider.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                Vector2 velocity = rb.velocity;
                velocity.y = jumpForce;
                rb.velocity = velocity;
            }

            Destroy(brokenPlatform);
            AudioSource.PlayClipAtPoint(jumpSound.clip, transform.position, 1f);
        }
    }
}
