﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcceptInstructions : MonoBehaviour
{
    private void Start()
    {
        Time.timeScale = 0;
    }
    public void ClearScreen()
    {
        Destroy(GameObject.Find("InstructionsCanvas(Clone)"));
        Time.timeScale = 1;

        if (!PlayerPrefs.HasKey("InstructionsRead"))
        {
            PlayerPrefs.SetInt("InstructionsRead", 1);
        }
    }
}
