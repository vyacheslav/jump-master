﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformProperties : MonoBehaviour
{
    bool hasEnemy = false;
    public GameObject enemyPrefab;

    // Update is called once per frame
    void Start()
    {
            if (PlayerPrefs.GetString("EnemiesToggle") == "True"
                || !PlayerPrefs.HasKey("EnemiesToggle"))
                if (!hasEnemy)
                    SpawnEnemy();
    }

    void SpawnEnemy()
    {
        if(Random.Range(0f, 100f) <= 10f)
        {
            hasEnemy = true;

            Transform platformPosition = gameObject.GetComponent(typeof(Transform)) as Transform;
            Vector3 enemyPosition = new Vector3();
            enemyPosition.y = platformPosition.position.y;
            enemyPosition.x = platformPosition.position.x;
            enemyPosition.y += 0.7f;

            Instantiate(enemyPrefab, enemyPosition, Quaternion.identity);
        }
    }
   
}
