﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    private void Start()
    {
        if (PlayerPrefs.HasKey("SoundToggle"))
            if (PlayerPrefs.GetString("SoundToggle") == "False")
                AudioListener.volume = 0f;
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Main");
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void GoToHighscore()
    {
        SceneManager.LoadScene("Highscore");
    }

    public void GoToOptionsMenu()
    {
        SceneManager.LoadScene("Options");
    }
}
