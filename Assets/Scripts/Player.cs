﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour
{

    private float movementSpeed = 7f;
    private float moveInput;
    //float movement = 0f;
    Rigidbody2D rb;
    Vector3 spawnPoint;
    public bool isDead = false;
    SpriteRenderer sr;
    private bool dontMove; // determine if we can move
    private bool moveLeft; // determine if we move left or right

    private float topScore = 0.0f;
    public Text scoreText;

 
    void Start()
    {
        // Check if player has read instructions before.
        // If player hasn't, then instantiate an instructiosn canvas.
        if (!PlayerPrefs.HasKey("InstructionsRead"))
        {
            Canvas instructionsCanvas = Resources.Load<Canvas>("InstructionsCanvas");
            instructionsCanvas.worldCamera = Camera.main;
            Instantiate(instructionsCanvas);
        }

        if (!PlayerPrefs.HasKey("GravitySlider"))
            this.GetComponent<Rigidbody2D>().gravityScale = 1.6f;
        else
        {
            switch (PlayerPrefs.GetInt("GravitySlider"))
            {
                case 1:
                    this.GetComponent<Rigidbody2D>().gravityScale = 1.2f;
                    break;
                case 2:
                    this.GetComponent<Rigidbody2D>().gravityScale = 1.6f;
                    break;
                case 3:
                    this.GetComponent<Rigidbody2D>().gravityScale = 2f;
                    break;
                default:
                    this.GetComponent<Rigidbody2D>().gravityScale = 1.6f;
                    break;
            }
        }
       
        rb = GetComponent<Rigidbody2D>();

        spawnPoint = transform.position;
        sr = GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;


        dontMove = true;
    }

   
    // Update is called once per frame
    void Update()
    {

        HandleMovement();

        // Update score
        if (rb.velocity.y > 0 && transform.position.y > topScore)
        {
            topScore = transform.position.y;
        }

        scoreText.text = "Score: " + Mathf.Round(topScore).ToString();
    }
   

    void FixedUpdate()
    {
        HandleMovement();
        //moveInput = Input.GetAxis("Horizontal");
        //rb.velocity = new Vector2(moveInput * movementSpeed, rb.velocity.y);

        //Update Sprite if jumping up
        if (rb.velocity.y > 0)
        {
            sr.sprite = Resources.Load<Sprite>("Art/dudeler_jump0");
        }
        else if (rb.velocity.y < 0)
        {
            sr.sprite = Resources.Load<Sprite>("Art/dudeler_jump1");
        }
    }

    void HandleMovement()
    {

        if (dontMove)
        {
            StopMoving();
        }
        else
        {

            if (moveLeft)
            {
                MoveLeft();
            }
            else if (!moveLeft)
            {
                MoveRight();
            }
        }
    }

    public void AllowMovement(bool canMove)
    {
        dontMove = false;
        moveLeft = canMove;
    }

    public void DontAllowMovement()
    {
        dontMove = true;
    }

    public void MoveLeft()
    {
        Vector2 velocity = rb.velocity;
        velocity.x = -movementSpeed;
        rb.velocity = velocity;
    }

    public void MoveRight()
    {
        Vector2 velocity = rb.velocity;
        velocity.x = movementSpeed;
        rb.velocity = velocity;
    }


    public void StopMoving()
    {
        Vector2 velocity = rb.velocity;
        velocity.x = 0f;
        rb.velocity = velocity;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "FallDetector")
        {
            isDead = true;
            //transform.position = spawnPoint;
            PlayerPrefs.SetFloat("CurrentScore", topScore);
            PlayerPrefs.SetFloat("CurrentX", transform.position.x);
            SceneManager.LoadScene("Death");
        }
    }
}
